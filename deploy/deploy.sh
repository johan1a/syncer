#!/bin/sh
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@urdatorn 'mkdir -p ~/syncer/data/'
scp -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa docker-compose.yml johan@urdatorn:syncer/
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@urdatorn 'cd syncer; docker-compose pull'
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@urdatorn 'cd; cd syncer/; env SYNCER_HOSTS=archlinux docker-compose up -d'

